package com.pruebas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebasMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebasMongodbApplication.class, args);
	}

}
