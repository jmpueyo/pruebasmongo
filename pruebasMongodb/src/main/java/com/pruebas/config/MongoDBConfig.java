package com.pruebas.config;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;

import com.mongodb.Block;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.SocketSettings;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class MongoDBConfig {

   @Bean
   public SimpleMongoClientDbFactory mongoClientDbFactory() {

      log.info("Construyendo cliente de mongo... ");
      MongoClient client = MongoClients.create(MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString("mongodb://hz:hz123@localhost:27017")) // Apuntar a mongodb 4.4
            .applyToSocketSettings(new Block<SocketSettings.Builder>() {
               @Override
               public void apply(final SocketSettings.Builder builder) {
                  builder.readTimeout(10000, TimeUnit.MILLISECONDS);
               }
            })
            .build());

      return new SimpleMongoClientDbFactory(client, "HZData");
   }

}
